var DataTypes = require("./DataTypes");
var os = require("os");

var PORT = 8000;
var HOST = '127.0.0.1';

var dgram = require('dgram');
var server = dgram.createSocket('udp4');

var globalposition = {};

server.on('listening', function () {
    var address = server.address();
    console.log('UDP Server listening on ' + address.address + ":" + address.port);
});

server.on('message', function (message, remote) {
    //console.log(remote.address + ':' + remote.port +' - ' + message);

    let header = message.slice(0, 5);
    let data = message.slice(5);
    let datasets = Math.floor(data.length / 36);

    for (let i = 0; i < datasets*36; i+=36) {

        let sentence = data.slice(i,i+36);
        let type_id = sentence[`readInt8`](0);
        let type = DataTypes.ds[type_id];
        let values = {};

        if (type) {
            let offset = 4;

            for ( let i = 0; i < type.data.length; i++ ) {

                var datapoint = type.data[i];

                if ( datapoint.type !== 'pad' ) {

                    values[datapoint.name] = sentence[`readFloat${os.endianness()}`](offset);
                    offset += 4

                } else {

                    offset += datapoint.length;

                }

            }

            // console.log({
            //     name: type.name,
            //     values: values
            // });

            if (type.name === 'globalposition') {
                globalposition = values;
            }

        }

    }

    //console.log(datasets, data, header);

});

server.bind(PORT, HOST);




var http = require('http');
var fs = require('fs');
var path = require('path');

http.createServer(function (request, response) {
    console.log('request ', request.url);

    var filePath = '.' + request.url;
    if (filePath == './') {
        filePath = './index.html';
    }

    if (filePath == './position.json') {
        response.writeHead(200, { 'Content-Type': 'application/json' });
        console.log(globalposition);
        response.end(JSON.stringify(globalposition), 'utf-8');
    }

    var extname = String(path.extname(filePath)).toLowerCase();
    var contentType = 'text/html';
    var mimeTypes = {
        '.html': 'text/html',
        '.js': 'text/javascript',
        '.css': 'text/css',
        '.json': 'application/json',
        '.png': 'image/png',
        '.jpg': 'image/jpg',
        '.gif': 'image/gif',
        '.wav': 'audio/wav',
        '.mp4': 'video/mp4',
        '.woff': 'application/font-woff',
        '.ttf': 'application/font-ttf',
        '.eot': 'application/vnd.ms-fontobject',
        '.otf': 'application/font-otf',
        '.svg': 'application/image/svg+xml'
    };

    contentType = mimeTypes[extname] || 'application/octet-stream';

    fs.readFile(filePath, function(error, content) {
        if (error) {
            if(error.code == 'ENOENT'){
                fs.readFile('./404.html', function(error, content) {
                    response.writeHead(200, { 'Content-Type': contentType });
                    response.end(content, 'utf-8');
                });
            }
            else {
                response.writeHead(500);
                response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
                response.end();
            }
        }
        else {
            response.writeHead(200, { 'Content-Type': contentType });
            response.end(content, 'utf-8');
        }
    });

}).listen(3000);